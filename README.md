# Ansible Role: Kubergrunt

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubergrunt/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubergrunt/-/commits/main)

This role installs [Kubergrunt](https://github.com/gruntwork-io/kubergrunt/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubergrunt_version: latest # v0.7.11 if you want a specific version
    kubergrunt_arch: amd64 # amd64, 386 or arm64
    setup_dir: /tmp
    kubergrunt_bin_path: /usr/local/bin/kubergrunt
    kubergrunt_repo_path: https://github.com/gruntwork-io/kubergrunt/releases/download

This role can install the latest or a specific version. See [available kubergrunt releases](https://github.com/gruntwork-io/kubergrunt/releases/) and change this variable accordingly.

    kubergrunt_version: latest # v0.7.11 if you want a specific version

The path of the Kubergrunt repository.

    kubergrunt_repo_path: https://github.com/gruntwork-io/kubergrunt/releases/download

The location where the kubergrunt binary will be installed.

    kubergrunt_bin_path: /usr/local/bin/kubergrunt

kubergrunt supports amd64, arm64 and 386 CPU architectures, just change for the main architecture of your CPU

    kubergrunt_arch: amd64 # amd64, 386 or arm64

Kubergrunt needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kubergrunt. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubergrunt

## License

MIT / BSD
